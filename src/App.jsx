import { useState } from 'react';
import './App.css';
import LoginPage from './LoginPage/LoginPage';
import HomePage from './HomePage/HomePage';
import ShopPage from './ShopPage/ShopPage';
import CartPage from './CartPage/CartPage';
import { BrowserRouter, Routes, Route, useNavigate } from "react-router-dom";
import UserProvider from "./Providers/UserProvider";
import GameCard from "./Components/GameCard";
import "bootstrap/dist/css/bootstrap.min.css"



function App() {
  return (
    <>
      <BrowserRouter>
        <UserProvider>
          <Routes>
            <Route path="/" element={<LoginPage/>}/>
            <Route path="/home" element={<HomePage/>}/>
            <Route path="/shop" element={<ShopPage/>}/>
            <Route path="/cart" element={<CartPage/>}/>
          </Routes>
        </UserProvider>
      </BrowserRouter>
    </>
  )
}

export default App
