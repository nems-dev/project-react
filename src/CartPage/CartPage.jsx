import { Link } from 'react-router-dom'
import { useContext, useState, useEffect } from "react";
import { UserContext } from "../Providers/UserProvider.jsx";
import GameCard from "../Components/GameCard"
import CustomNavBar from '../Components/CustomNavBar'
import Button from "react-bootstrap/Button"
function CartPage() {
    const {user, token, shopObj, setShopObj, setToken} = useContext(UserContext);
    const totalPrice = shopObj.data.filter((value)=> value.state==="onCart").reduce((agg,value)=>{return agg + value.price;},0)
    useEffect(() => {document.title = "Comshop - Cart"},[])      
    const showGames = shopObj.isLoading == false ? shopObj.data.map((value, index) => {
        if(value.state === "onCart") {
            return(
                <>
                    <GameCard key={index} props={value}/>
                </>
            )
        }
    }) : "";
    const checkoutHandler = (event) => {
        const diff = token - totalPrice
        if (diff >= 0 && totalPrice != 0){
            setToken(diff)
            const newShopObj = shopObj.data.map((value) =>{
                if (value.state == "onCart"){
                    return {...value, state: "onLibrary"}
                }
                return value
            })
            setShopObj({data:newShopObj, isLoading: false, error: null});
            alert("Successfully checked out total: " + totalPrice)
        }
        else if (diff < 0){
            alert("Not enough funds. Top up more tokens to checkout.")
        }
    }
    return(
        <>
        <CustomNavBar/>
        <div style={{margin:"50px 50px 50px 50px"}}>
            <h1 style={{marginTop:"80px", marginBottom:"20px"}}>My Cart</h1>
            <h4 style={{marginBottom:"20px"}}>Total price: {totalPrice}</h4>
            <Button variant="success" className="dev-button" onClick={checkoutHandler}>Checkout</Button>
            <div style={{display:"flex",flexWrap:"wrap",flexDirection:"row", justifyContent:"center"}}>
                {showGames}
            </div>

        </div>

        </>
    )
}
export default CartPage;