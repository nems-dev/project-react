import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import NavDropdown from 'react-bootstrap/NavDropdown'
import { useContext, useState } from "react";
import { UserContext } from "../Providers/UserProvider.jsx";
import { Link } from "react-router-dom"
import '../App.css';

function CustomNavBar() {
    const {user,token} = useContext(UserContext);
    const [isHovered, setIsHovered] = useState(false);
    const handleMouseOver = () => {
        setIsHovered(true);
    };
    
    const handleMouseOut = () => {
        setIsHovered(false);
    };
    const opacityVal = isHovered ? '0' : '1';
    const reverseVal = isHovered ? '1' : '0';

    return(
        <>
            <Navbar expand="lg" className="bg-body-tertiary" data-bs-theme="dark" fixed="top">
            <Container>
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav">
                <Nav className="me-auto">
                    <Nav.Link as={Link} to={{pathname:'/home'}}>Home</Nav.Link>
                    <Nav.Link as={Link} to={{pathname:'/shop'}}>Shop</Nav.Link>
                    <Nav.Link as={Link} to={{pathname:'/cart'}}>Cart</Nav.Link>
                </Nav>
                </Navbar.Collapse>
                <Navbar.Brand className="d-none d-lg-block" style={{position:"absolute" ,left:"50%", transform: "translateX(-50%)"}}>
                    <div style={{display:"flex", flexDirection:"row", alignItems:"center"}}>
                        <h4>COMSHOP GAME STORE</h4>
                    </div>  
                </Navbar.Brand>
                <Nav.Link as={Link} to={{pathname:'/'}}  style={{margin:"10px",opacity: reverseVal}} onMouseOver={handleMouseOver} onMouseOut={handleMouseOut}>Log Out</Nav.Link>
                <Navbar.Text className="d-none d-lg-block" style={{margin:"10px"}}>user: <i>{user}</i> </Navbar.Text>
                <Navbar.Text style={{margin:"10px"}}>token: <b>{token}</b></Navbar.Text>
                <Navbar.Text style={{margin:"10px",opacity: opacityVal}} onMouseOver={handleMouseOver} onMouseOut={handleMouseOut}>Log out</Navbar.Text>

            </Container>
            </Navbar>
        </>
    )
}
export default CustomNavBar;