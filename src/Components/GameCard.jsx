import { UserContext } from "../Providers/UserProvider.jsx";
import { useContext, useEffect, useState } from "react";
import Card from "react-bootstrap/Card"
import Button from "react-bootstrap/Button"
import Image from "react-bootstrap/Image"
import {Fade} from "react-awesome-reveal"
const GameCard = ({props}) => {
const {gid, name, state, price,img} = props
console.log(props)
const {shopObj, setShopObj, token, setToken} = useContext(UserContext);
const testHandler = (event) => {
    const newShopObj = shopObj.data.map((value) =>{
        // console.log(event.target)
        if(value.gid === parseInt(event.target.value)){
            if (value.state == "onShop"){
                return {...value, state: "onCart"};
            }
            else if (value.state == "onCart") {
                return {...value, state: "onShop"};
            }
            else {
                // console.log(event.target.value)
                alert("Refund will only get 50% of original value. Tokens to be added: " + parseInt(value.price)*0.5)
                setToken(token + parseInt(value.price)*0.5)
                
                return {...value, state: "onShop"};
            }
        }
        return value;
    })
    setShopObj({data:newShopObj, isLoading: false});
}
const shopButton = state === "onShop" ? "to Cart"  : (state==="onCart" ? "Remove": "Refund");
return(
    <>
    <Fade>
    <Card data-bs-theme="dark" style={{width: "300px", height: "300px", margin:"10px 10px 10px 10px"}}>
        <Card.Img variant="top" src={img}/>
        <Card.Body>
            <Card.Title style={{margin:"10px 10px 10px 10p", fontSize:"17px" }}>{name}</Card.Title>
            <Card.Subtitle style={{fontSize:"14px"}}>{price} Tokens</Card.Subtitle>
            <Button variant="outline-info" onClick={testHandler} value={gid} style={{margin:"10px 10px 10px 10px",fontSize:"15px"}}>{shopButton}</Button>
        </Card.Body>
    </Card>
    </Fade>
    </>
)
}
export default GameCard;