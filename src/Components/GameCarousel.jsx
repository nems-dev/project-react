import Carousel from 'react-bootstrap/Carousel';
// import ExampleCarouselImage from 'components/ExampleCarouselImage';
import Image from 'react-bootstrap/Image';
import {useNavigate} from 'react-router-dom';
import {useState} from 'react';
function GameCarousel() {
    const [isHovered, setIsHovered] = useState(false);
    const navigate = useNavigate();
    const imageHandler = (event) => {
        navigate("/shop")
    }
    const handleMouseOver = () => {
        setIsHovered(true);
      };
    
      const handleMouseOut = () => {
        setIsHovered(false);
      };
    const cursorStyle = isHovered ? 'pointer' : 'default';
    return (
        <Carousel data-bs-theme="light">
        <Carousel.Item interval={1500}>
            <Image src="/images/baldursgate.jpg" onClick={imageHandler} onMouseOver={handleMouseOver} onMouseOut={handleMouseOut} style={{ cursor: cursorStyle }}/>
            <Carousel.Caption>
                <div style={{backgroundColor:"rgba(0,0,0,0.6)"}}>
                    <h3>New Release</h3>
                    <p style={{color:"yellow"}}>4.7/5 stars!</p>
                </div>
            </Carousel.Caption>
        </Carousel.Item>
        <Carousel.Item interval={1500}>
            <Image src="/images/witcher3.jpg" onClick={imageHandler} onMouseOver={handleMouseOver} onMouseOut={handleMouseOut} style={{ cursor: cursorStyle }} />
            <Carousel.Caption>
                <div style={{backgroundColor:"rgba(0,0,0,0.6)"}}>
                    <h3>Game of the year!</h3>
                    <p style={{color:"yellow"}}>4.5/5 stars!</p>
                </div>
            </Carousel.Caption>
        </Carousel.Item>
        <Carousel.Item interval={1500}>
        <Image src="/images/civilization.jpg" onClick={imageHandler} onMouseOver={handleMouseOver} onMouseOut={handleMouseOut} style={{ cursor: cursorStyle }}/> 
            <Carousel.Caption>
                <div style={{backgroundColor:"rgba(0,0,0,0.6)"}}>
                    <h3>Comes with bonus DLC!</h3>
                    <p style={{color:"yellow"}}>4.5/5 stars!</p>
                </div>
            </Carousel.Caption>
        </Carousel.Item>
        </Carousel>
    );
    }

    export default GameCarousel;