import { useState, useEffect } from 'react';
function NumberCounter() {
    const [count, setCount] = useState(0);
    const [targetNumber, setTargetNumber] = useState(2134); // Set your desired target number here
    useEffect(() => {
      let interval;
      let skip = 10;
      let timems = 10;
      if (count < targetNumber) {
        interval = setInterval(() => {
            if((targetNumber-count)>skip){
                setCount((prevCount) => prevCount + skip);
            }
            else{
                setCount(targetNumber)
            }
        }, timems);
      }
      return () => clearInterval(interval);
    }, [count, targetNumber]);
  
    return (
      <div>
        {/* <h1>Number Counter</h1> */}
        <h2>{count}</h2>
        {/* {count === targetNumber ? (
          <p>Target number reached!</p>
        ) : (
          <p>Counting to {targetNumber}...</p>
        )} */}
      </div>
    );
  }
  
  export default NumberCounter;