import { useEffect, useState } from 'react';
import GameCard from "../Components/GameCard"
import GameCarousel from "../Components/GameCarousel"
import { useContext } from "react";
import { UserContext } from "../Providers/UserProvider.jsx";
import CustomNavBar from '../Components/CustomNavBar'
import Button from 'react-bootstrap/Button'
import '../App.css';
function HomePage() {
    const {user,token,shopObj,setToken} = useContext(UserContext);
    const [loadToken, setLoadToken] = useState(100);
    useEffect(()=> {document.title = "Comshop - Home"},[])
    const showGames = shopObj.isLoading == false ? shopObj.data.map((value, index) => {
        if(value.state === "onLibrary") {
            return(
                <>
                    <GameCard key={index} props={value}/>
                </>
            )
        }
    }) : "";
    const inputHandler = (event) => {
        setLoadToken(event.target.value)
    }
    const addTokenHandler = () => {
        setToken(parseInt(token) + parseInt(loadToken))
        alert("Successfully top up: " + loadToken + "!")
    }
    return(
        <>
        <CustomNavBar/>
        <div className="dev-homepage" style={{margin:"50px 50px 50px 50px"}}>
            <h1 style={{marginTop:"80px"}}>Welcome {user}!</h1>
        </div>
        <h2>Featured Games</h2>
        <GameCarousel/>
        <div style={{margin:"50px 50px 50px 50px"}}>
            <h2>Load Tokens</h2>
            <input type="text" onChange={inputHandler} value={loadToken}></input>
            <br/>
            <Button onClick={addTokenHandler} variant="dark" style={{margin:"10px 10px 10px 10px"}} >Topup</Button>
        </div >
        <div style={{margin:"50px 50px 50px 50px"}}>
            <h2>Game Library</h2>
            <div style={{display:"flex",flexWrap:"wrap",flexDirection:"row", justifyContent:"center"}}>
                {showGames}
            </div>
        </div>
        </>
    )
}
export default HomePage;