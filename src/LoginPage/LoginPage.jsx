import { useNavigate } from "react-router-dom";
import useFetchJson from "../Hooks/useFetchJson.jsx";
import { useState, useEffect } from 'react';
import { useContext } from "react";
import { UserContext } from "../Providers/UserProvider.jsx";
import Card from "react-bootstrap/Card";
import Button from 'react-bootstrap/Button';
import NumberCounter from '../Components/NumberCounter';
import '../App.css';

function LoginPage() {
    const {setUser,setToken,setShopObj, shopObj} = useContext(UserContext);
    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");
    const [open, setOpen] = useState(true);
    const [timeAppear, setTimeAppear] = useState(5000);

    const [errorMessage, setErrorMessage] = useState("");
    const [userInfo, setUserInfo] = useState("");
    const navigate = useNavigate();
    const userdata = useFetchJson('./User.json');
    const shopAsyncData = useFetchJson("./ShopItems.json")
    useEffect(()=> {document.title = "Comshop"},[])
    useEffect(() => {setUserInfo(userdata)},[userdata.isLoading])
    useEffect(() => {setShopObj(shopAsyncData)},[shopAsyncData.isLoading])
    const setUsernameHandler = (event) => {
        setUsername(event.target.value);
    }
    const setPasswordHandler = (event) => {
        setPassword(event.target.value);
        console.log(password)
        console.log(userInfo.data.userpassword)
    }

    const loginButtonHandler = (event) => {
        event.preventDefault();
        if (userInfo.isLoading == false){
            if (userInfo.data.username === username && userInfo.data.userpassword === password){
                setUser(username)
                setToken(userInfo.data.tokens)
                navigate("/home")
            }
            else{
                setErrorMessage("Wrong username or password")
            }
        }
    }
    return (
        <>
        <div style={{display:"flex", flexDirection:"column", height:"70vh", alignItems: "center", justifyContent: "center",marginTop:"50px"}}>
            <Card className="fadein" data-bs-theme="dark" style={{width: '18rem'}}>
                <Card.Img variant="top" src="/comshoplogo.jpg" style={{borderRadius:"15px"}}/>
                <Card.Title>
                </Card.Title>
                <Card.Body>
                    <label>Username</label>
                    <input type="text" onChange={setUsernameHandler} value={username}></input>
                    <br/>
                    <label>Password</label>
                    <br/>
                    <input type="password" onChange={setPasswordHandler} value={password}></input>
                    <br/>
                    <p style={{color:"red",fontSize:"12px"}}>{errorMessage}</p>
                </Card.Body>
                <Button onClick={loginButtonHandler}  style={{width:"100px", margin:"auto auto"}}>Log In</Button>
             </Card>
             <br/>
            <h2>Active Players</h2>
            <NumberCounter/>
        </div>
        </>
    )
}

export default LoginPage;