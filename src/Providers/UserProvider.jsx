import { useState, createContext } from "react";
import useFetchJson from "../Hooks/useFetchJson.jsx";

export const UserContext = createContext();

const UserProvider = ({children}) => {
    const initData = {data: null, isLoading: true, error: null};
    const [shopObj, setShopObj] = useState(initData);
    const [user, setUser] = useState("test");
    const [token, setToken] = useState("0");
    const settings = {
        user,
        setUser,
        token,
        setToken,
        shopObj,
        setShopObj,
    };
    return (
        <UserContext.Provider value={settings}>{children}</UserContext.Provider>
    );
};

export default UserProvider;