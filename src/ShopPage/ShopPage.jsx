import { Link } from 'react-router-dom'
import { useContext, useEffect, useState } from "react";
import { UserContext } from "../Providers/UserProvider.jsx";
import useFetchJson from "../Hooks/useFetchJson.jsx";
import GameCard from "../Components/GameCard"
import CustomNavBar from '../Components/CustomNavBar'
import Container from 'react-bootstrap/Container'
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button';
function ShopPage() {
    const {user, token, shopObj, setShopObj} = useContext(UserContext);
    const [findValue, setFindValue] = useState("")
    useEffect(() => {document.title = "Comshop - Shop"},[])      
    const shopAsyncData = useFetchJson("./ShopItems.json")
    // useEffect(() => {setShopObj(shopAsyncData)},shopAsyncData.isLoaded)
    const findValueHandler = (event) => {
        event.preventDefault();
        setFindValue(event.target.value);
        console.log(findValue)
    }
    const clearValueHandler = (event) => {
        event.preventDefault();
        setFindValue("");
        console.log(findValue);
    }
    // console.log(shopObj)
    const showGames = shopObj.isLoading == false ? shopObj.data.filter((value)=> {return value.name.toLowerCase().includes(findValue.toLowerCase());}).map((value, index) => {
        if(value.state === "onShop") {
            return(
                <>
                    <GameCard key={index} props={value}/>
                </>
            )
        }
    }) : "";
    return(
        <>  
            <CustomNavBar/>
            <div style={{margin:"50px 50px 50px 50px"}}>
                <h1 style={{marginTop:"80px"}}>Game Shop</h1>
                <Form>
                    <Form.Group>
                        <Form.Label style={{margin:"10px"}}>Search Shop</Form.Label>
                        <Form.Control type="text" placeholder="Name of game" onChange={findValueHandler} value={findValue} style={{width:"200px", margin:"auto auto"}}/>
                    </Form.Group>
                </Form>
                <Button variant= "secondary" onClick={clearValueHandler} style={{marginBottom:"50px", marginTop:"20px"}}>Clear Search</Button>

                <div style={{display:"flex",flexWrap:"wrap",flexDirection:"row", justifyContent:"center"}}>
                    {showGames}
                </div>
            </div>
        </>
    )
}
export default ShopPage;